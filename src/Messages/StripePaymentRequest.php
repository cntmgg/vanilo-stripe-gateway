<?php

declare(strict_types=1);

namespace Vanilo\Stripe\Messages;

use Illuminate\Support\Facades\View;
use Stripe\Customer as StripeCustomer;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Vanilo\Payment\Contracts\PaymentRequest;
use Vanilo\Stripe\Concerns\HasStripeConfiguration;

class StripePaymentRequest implements PaymentRequest
{
    use HasStripeConfiguration;

    private string $paymentId;

    private string $currency;

    private float $amount;

    public ?PaymentIntent $paymentIntent;

    private ?StripeCustomer $stripeCustomer;

    private ?string $returnUrl = null;

    private string $view = 'stripe::_request';

    public function getHtmlSnippet(array $options = []): ?string
    {
        return View::make(
            $this->view,
            [
                'publicKey' => $this->publicKey,
                'intentSecret' => $this->paymentIntent->client_secret,
                'returnUrl' => $this->returnUrl
            ]
        )->render();
    }

    public function willRedirect(): bool
    {
        return true;
    }

    public function createPaymentIntent(): self
    {
        Stripe::setApiKey($this->secretKey);
        $this->paymentIntent = PaymentIntent::create($this->getPaymentIntentOptions());

        return $this;
    }

    public function getPaymentIntentOptions(): array
    {
        return [
            'amount' => $this->amount * 100,
            'currency' => $this->currency,
            'customer' => $this->stripeCustomer ?? null,
            'metadata' => [
                'payment_id' => $this->paymentId
            ]
        ];
    }

    public function setSecretKey(string $secretKey): self
    {
        $this->secretKey = $secretKey;

        return $this;
    }

    public function setPublicKey(string $publicKey): self
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    public function setPaymentId(string $paymentId): self
    {
        $this->paymentId = $paymentId;

        if (isset($this->paymentIntent)) {
            $this->paymentIntent = PaymentIntent::update($this->paymentId, $this->getPaymentIntentOptions());
        }
        return $this;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        if (isset($this->paymentIntent)) {
            $this->paymentIntent = PaymentIntent::update($this->paymentId, $this->getPaymentIntentOptions());
        }
        return $this;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        if (isset($this->paymentIntent)) {
            $this->paymentIntent = PaymentIntent::update($this->paymentId, $this->getPaymentIntentOptions());
        }
        return $this;
    }

    public function setView(string $view): self
    {
        $this->view = $view;

        return $this;
    }

    public function setReturnUrl(string $returnUrl): self
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }

    public function setCustomer(StripeCustomer $stripeCustomer): self
    {
        $this->stripeCustomer = $stripeCustomer;

        return $this;
    }
}
