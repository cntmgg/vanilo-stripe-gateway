<?php

declare(strict_types=1);

namespace Vanilo\Stripe\Factories;

use Vanilo\Payment\Contracts\Payment;
use Vanilo\Stripe\Concerns\HasStripeInteraction;
use Vanilo\Stripe\Concerns\ManagesCustomer;
use Vanilo\Stripe\Messages\StripePaymentRequest;

final class RequestFactory
{
    use HasStripeInteraction;

    public function create(Payment $payment, array $options = []): StripePaymentRequest
    {
        $result = new StripePaymentRequest();

        $result
            ->setSecretKey($this->secretKey)
            ->setPublicKey($this->publicKey)
            ->setPaymentId($payment->getPaymentId())
            ->setCurrency($payment->getCurrency())
            ->setAmount($payment->getAmount());

        $user = auth()->user();
        if ($user instanceof \Vanilo\Stripe\Contracts\ManagesCustomer) {
            $stripeCustomer = $user->createOrGetStripeCustomer();
            $result->setCustomer($stripeCustomer);
        }

        if (isset($options['return_url'])) {
            $result->setReturnUrl($options['return_url']);
        }

        if (isset($options['view'])) {
            $result->setView($options['view']);
        }

        $result->createPaymentIntent();

        $payment->update([
            'remote_id' => $result->paymentIntent->id,
        ]);

        return $result;
    }
}
