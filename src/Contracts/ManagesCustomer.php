<?php

namespace Vanilo\Stripe\Contracts;

use Stripe\Customer as StripeCustomer;

interface ManagesCustomer
{
    /**
     * Retrieve the Stripe customer ID.
     *
     * @return string|null
     */
    public function stripeId(): ?string;

    /**
     * Get the name that should be synced to Stripe.
     *
     * @return string|null
     */
    public function stripeName(): ?string;

    /**
     * Get the email address that should be synced to Stripe.
     *
     * @return string|null
     */
    public function stripeEmail(): ?string;

    /**
     * Get the phone number that should be synced to Stripe.
     *
     * @return string|null
     */
    public function stripePhone(): ?string;

    /**
     * Get the address that should be synced to Stripe.
     *
     * @return array|null
     */
    public function stripeAddress(): ?array;

    /**
     * Get the locales that should be synced to Stripe.
     *
     * @return array|null
     */
    public function stripePreferredLocales(): ?array;

    /**
     * Create a Stripe customer for the given model.
     *
     * @param  array  $options
     * @return \Stripe\Customer
     *
     * @throws \Vanilo\Stripe\Exceptions\CustomerAlreadyCreated
     */
    public function createAsStripeCustomer(array $options = []): StripeCustomer;

    /**
     * Get the Stripe customer instance for the current user or create one.
     *
     * @param  array  $options
     * @return StripeCustomer
     */
    public function createOrGetStripeCustomer(array $options = []): StripeCustomer;

    /**
     * Get the Stripe customer for the model.
     *
     * @param  array  $expand
     * @return StripeCustomer
     */
    public function asStripeCustomer(array $expand = []): StripeCustomer;


}