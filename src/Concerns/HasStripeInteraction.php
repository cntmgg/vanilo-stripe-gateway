<?php

declare(strict_types=1);

namespace Vanilo\Stripe\Concerns;

use Stripe\StripeClient;

trait HasStripeInteraction
{
    use HasStripeConfiguration;

    public function __construct(string $secretKey, string $publicKey)
    {
        $this->secretKey = $secretKey;
        $this->publicKey = $publicKey;
    }

    /**
     * Get the Stripe SDK client.
     *
     * @param  array  $options
     * @return \Stripe\StripeClient
     */
    public function stripe(array $options = [])
    {
        return new StripeClient(array_merge([
            'api_key' => $options['api_key'] ?? $this->secretKey,
        ], $options));
    }
}
