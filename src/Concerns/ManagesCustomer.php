<?php

namespace Vanilo\Stripe\Concerns;

use Stripe\Customer as StripeCustomer;
use Stripe\StripeClient;
use Vanilo\Stripe\Exceptions\CustomerAlreadyCreated;
use Vanilo\Stripe\Exceptions\InvalidCustomer;
use Vanilo\Stripe\StripePaymentGateway;

trait ManagesCustomer
{
    /**
     * Retrieve the Stripe customer ID.
     *
     * @return string|null
     */
    public function stripeId(): ?string
    {
        return $this->stripe_id;
    }

    /**
     * Determine if the customer has a Stripe customer ID.
     *
     * @return bool
     */
    public function hasStripeId(): bool
    {
        return ! is_null($this->stripe_id);
    }

    /**
     * Determine if the customer has a Stripe customer ID and throw an exception if not.
     *
     * @return void
     *
     * @throws \Vanilo\Stripe\Exceptions\InvalidCustomer
     */
    protected function assertCustomerExists(): void
    {
        if (! $this->hasStripeId()) {
            throw InvalidCustomer::notYetCreated($this);
        }
    }

    /**
     * Create a Stripe customer for the given model.
     *
     * @param  array  $options
     * @return \Stripe\Customer
     *
     * @throws \Vanilo\Stripe\Exceptions\CustomerAlreadyCreated
     */
    public function createAsStripeCustomer(array $options = []): StripeCustomer
    {
        if ($this->hasStripeId()) {
            throw CustomerAlreadyCreated::exists($this);
        }

        if (! array_key_exists('name', $options) && $name = $this->stripeName()) {
            $options['name'] = $name;
        }

        if (! array_key_exists('email', $options) && $email = $this->stripeEmail()) {
            $options['email'] = $email;
        }

        if (! array_key_exists('phone', $options) && $phone = $this->stripePhone()) {
            $options['phone'] = $phone;
        }

        if (! array_key_exists('address', $options) && $address = $this->stripeAddress()) {
            $options['address'] = $address;
        }

        if (! array_key_exists('preferred_locales', $options) && $locales = $this->stripePreferredLocales()) {
            $options['preferred_locales'] = $locales;
        }

        // Here we will create the customer instance on Stripe and store the ID of the
        // user from Stripe. This ID will correspond with the Stripe user instances
        // and allow us to retrieve users from Stripe later when we need to work.
        $customer = $this->stripe()->customers->create($options);

        $this->stripe_id = $customer->id;

        $this->save();

        return $customer;
    }

    /**
     * Get the Stripe customer instance for the current user or create one.
     *
     * @param  array  $options
     * @return StripeCustomer
     */
    public function createOrGetStripeCustomer(array $options = []): StripeCustomer
    {
        if ($this->hasStripeId()) {
            return $this->asStripeCustomer();
        }

        return $this->createAsStripeCustomer($options);
    }

    /**
     * Get the Stripe customer for the model.
     *
     * @param  array  $expand
     * @return StripeCustomer
     */
    public function asStripeCustomer(array $expand = []): StripeCustomer
    {
        $this->assertCustomerExists();

        return $this->stripe()->customers->retrieve(
            $this->stripe_id, ['expand' => $expand]
        );
    }

    /**
     * Get the name that should be synced to Stripe.
     *
     * @return string|null
     */
    public function stripeName(): ?string
    {
        return $this->name;
    }

    /**
     * Get the email address that should be synced to Stripe.
     *
     * @return string|null
     */
    public function stripeEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Get the phone number that should be synced to Stripe.
     *
     * @return string|null
     */
    public function stripePhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Get the address that should be synced to Stripe.
     *
     * @return array|null
     */
    public function stripeAddress(): ?array
    {
        // return [
        //     'city' => 'Little Rock',
        //     'country' => 'US',
        //     'line1' => '1 Main St.',
        //     'line2' => 'Apartment 5',
        //     'postal_code' => '72201',
        //     'state' => 'Arkansas',
        // ];
        return [];
    }

    /**
     * Get the locales that should be synced to Stripe.
     *
     * @return array|null
     */
    public function stripePreferredLocales(): ?array
    {
        // return ['en'];
        return [];
    }

    /**
     * Get the Stripe SDK client.
     *
     * @param  array  $options
     * @return StripeClient
     */
    public static function stripe(array $options = []): StripeClient
    {
        return app(StripePaymentGateway::class)::stripe($options);
    }
}